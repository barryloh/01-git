const Chuck = require('chucknorris-io');

const client = new Chuck();

const generateJoke = () => {
  return new Promise((resolve, reject) => {
    client
      .getRandomJoke('dev')
      .then(joke => {
        resolve(joke.value);
      })
      .catch(err => {
        console.error(err);
        reject(err);
      });
  });
};

// Get process.stdin as the standard input object.
const standard_input = process.stdin;

// Set input character encoding.
standard_input.setEncoding('utf-8');

// Prompt user to input data in console.
console.log('Do you wanna hear a joke? (Y/N)');

// When user input data and click enter key.
standard_input.on('data', data => {
  if (data === 'Y\n' || data === 'y\n') {
    // Retrieve a random chuck joke
    generateJoke()
      .then(response => {
        console.log(`😂  ${response}`);
        console.log('\nDo you wanna hear another joke? (Y/N)');
      })
      .catch(function(err) {
        console.error(err);
      });
  } else if (data === 'N\n' || data === 'n\n') {
    process.exit();
  } else {
    console.log('Me not understanding. Please try again!');
  }
});
